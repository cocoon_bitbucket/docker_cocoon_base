cocoon/base
===========

an ansible ready docker base image based on  debian:wheezy

contains:

* python
* ansible
* git
* vim + vimrc syntax hightlight


